#!/bin/bash

set -e

# Execute Artifactory
if [ "$1" = "run" ]; then
	chown -R artifactory ${ARTIFACTORY_HOME}/data ${ARTIFACTORY_HOME}/backup ${ARTIFACTORY_HOME}/logs
  	shift 1
  	exec gosu artifactory ${ARTIFACTORY_HOME}/bin/artifactory.sh "$@"
fi

exec "$@"
