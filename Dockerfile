FROM raynera/java
MAINTAINER Andrew Rayner <a.d.rayner@gmail.com>

ENV ARTIFACTORY_VERSION 3.6.0
ENV ARTIFACTORY_DOWNLOAD_URL https://bintray.com/artifact/download/jfrog/artifactory/artifactory-${ARTIFACTORY_VERSION}.zip
ENV ARTIFACTORY_HOME /artifactory

RUN curl -L -o artifactory.zip ${ARTIFACTORY_DOWNLOAD_URL} \
	&& unzip artifactory.zip \
	&& rm artifactory.zip \
	&& mv artifactory-${ARTIFACTORY_VERSION} ${ARTIFACTORY_HOME}

COPY entrypoint.sh /entrypoint.sh

RUN useradd -d ${ARTIFACTORY_HOME} -u 1000 -m -s /bin/bash artifactory \
	&& chown -R artifactory ${ARTIFACTORY_HOME} \
	&& chmod +x /entrypoint.sh

VOLUME ["${ARTIFACTORY_HOME}/data", "${ARTIFACTORY_HOME}/backup", "${ARTIFACTORY_HOME}/logs"]

EXPOSE 8081 8019

ENTRYPOINT ["/entrypoint.sh"]
CMD ["run"]
