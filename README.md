# What is Artifactory?

[Artifactory](http://www.jfrog.com/open-source/#os-arti) is an open source artifact repository system developed by [JFrog](http://www.jfrog.com/)

![logo](http://upload.wikimedia.org/wikipedia/en/d/d1/JFrog-Logo-trans-cut.png)


## Quick Start

Enter the following to get Artifactory up and started quickly:

```bash
docker run -it --rm --publish 8081:8081 raynera/artifactory
```

## Volumes

Volumes created are as follows:

 * `/artifactory/data`
 * `/artifactory/backup`
 * `/artifactory/logs`

## Ports

The following ports are opened from within the container on startup:
 
  * `8081` - Artifactory Web Application
  * `8019` - [AJP](http://en.wikipedia.org/wiki/Apache_JServ_Protocol) port
 